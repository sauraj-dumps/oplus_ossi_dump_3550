#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery:134217728:4dcaff554a5dd1405ff6c1189db7cf3bba7a5cf5; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot:33554432:d9afa4cf186e47fb8983b74a840c0ef603bc3459 \
          --target EMMC:/dev/block/by-name/recovery:134217728:4dcaff554a5dd1405ff6c1189db7cf3bba7a5cf5 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
